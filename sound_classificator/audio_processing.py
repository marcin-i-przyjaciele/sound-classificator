import os
import json
import struct
import librosa as lr
import pandas as pd
import numpy as np
from typing import List, Tuple

from key_info_extraction import extract_features


def load_data(metadata_df: pd.DataFrame) -> pd.DataFrame:
    """Loads audio data from files listed in metadata_df, 
    data is resampled to 44100 Hz, converted to mono,also adjusts audio length to 4s
    
    Returns pd.DataFrame with audio data and classID from metadata_df"""
    metadata_df = add_proper_paths(metadata_df)
    metadata_df = add_audio_information(metadata_df)

    audio_series = load_audio_files(metadata_df)
    audio_series = adjust_audio_length(audio_series)

    metadata_columns = ["salience", "fold", "classID", "class"]
    data_df = pd.concat(
        [audio_series.rename("audio"), metadata_df[metadata_columns]], axis=1
    )

    return data_df


def adjust_audio_length(audio_series: pd.Series) -> pd.Series:
    """Replaces every audio np.array in given pd.Series with 4s for sampling rate 44100 Hz.
    Shorter audio series than 4s are filled with silence, longer are replaced by their first 4s"""

    audio_series = audio_series.apply(_adjust_audio_length)
    return audio_series


def _adjust_audio_length(audio: np.array) -> np.array:
    """Replaces audio in np.array with 4s for sampling rate 44100 Hz.
    Shorter audio series than 4s are filled with silence, longer are replaced by their first 4s"""

    if len(audio) > 176400:
        audio = audio[:176400]
    elif len(audio) < 176400:
        audio = np.concatenate((audio, np.zeros(176400 - len(audio))), axis=0)
    return audio


def load_audio_files(metadata_df: pd.DataFrame) -> pd.Series:
    """Returns pd.Series filled with audio data for each row in metadata_df"""

    audio = metadata_df.apply(_load_audio, axis=1)
    return audio


def _load_audio(row: pd.Series) -> np.array:
    """Returns np.array with audio data for given row"""

    return _load_file(row["file_path"])


def _load_file(file_path: str) -> np.array:
    """Internal function to load audio from wav file, and resample it to 44100 Hz and converts to mono"""

    (y, _) = lr.core.load(file_path, sr=44100, mono=True)
    return y


def add_audio_information(metadata_df: pd.DataFrame) -> pd.DataFrame:
    """Adds `num_channels`, `sample_rate` and `bit_depth` to each row in metadata_df"""

    metadata_df = metadata_df.apply(_append_audio_information, axis=1)
    return metadata_df


def _append_audio_information(row: pd.Series) -> pd.Series:
    """Adds `num_channels`, `sample_rate` and `bit_depth` to given row"""

    num_channels, sample_rate, bit_depth = _read_audio_information(row["file_path"])
    row["num_channels"] = num_channels
    row["sample_rate"] = sample_rate
    row["bit_depth"] = bit_depth
    return row


def _read_audio_information(filename: str) -> Tuple:
    """Internal function to read information from .wav header"""

    wave_file = open(filename, "rb")

    riff = wave_file.read(12)
    fmt = wave_file.read(36)

    num_channels_string = fmt[10:12]
    num_channels = struct.unpack("<H", num_channels_string)[0]

    sample_rate_string = fmt[12:16]
    sample_rate = struct.unpack("<I", sample_rate_string)[0]

    bit_depth_string = fmt[22:24]
    bit_depth = struct.unpack("<H", bit_depth_string)[0]

    return num_channels, sample_rate, bit_depth


def add_proper_paths(metadata_df: pd.DataFrame) -> pd.DataFrame:
    """Adds absolute file paths to metadata_df"""

    metadata_df["file_path"] = metadata_df.apply(_find_proper_path, axis=1)
    return metadata_df


def _find_proper_path(row: pd.Series) -> str:
    """Internal function to join proper path from row."""

    return os.path.join(
        os.path.abspath("UrbanSound8K/audio/"),
        "fold" + str(row["fold"]) + "/",
        str(row["slice_file_name"]),
    )


def replace_audio_with_stats(
    audio_df: pd.DataFrame, key_informations: List[str] = None
) -> Tuple[pd.DataFrame, List[str], List[str]]:
    """
    Returns calculated audio statistics along with Input columns and Output columns
    """
    stats = extract_features(audio_df, key_informations)
    audio_df = audio_df.drop("audio", axis=1)
    return stats.join(audio_df), stats.columns.to_list(), audio_df.columns.to_list()


def preprocess_audio_in_batches(
    metadata_df: pd.DataFrame,
    number_of_batches: int = 10,
    key_informations: List[str] = None,
):
    start_time = pd.Timestamp.now().floor("S")
    save_dir = os.path.join(os.getcwd(), "preprocessed_data")
    os.makedirs(save_dir, exist_ok=True)

    chunk_size = int(len(metadata_df) / number_of_batches)

    for batch in range(number_of_batches):
        audio_df = load_data(metadata_df[batch * chunk_size : (batch + 1) * chunk_size])
        audio_df, input_columns, info_columns = replace_audio_with_stats(
            audio_df, key_informations
        )
        audio_df.to_csv(f"{save_dir}/batch_{batch}.csv", index_label="index")
        del audio_df

    final_df = pd.concat(
        [
            pd.read_csv(f"{save_dir}/batch_{batch}.csv", index_col="index")
            for batch in range(number_of_batches)
        ]
    )
    final_df.to_csv(f"{save_dir}/full_dataset.csv", index_label="index")

    end_time = pd.Timestamp.now().floor("S")

    with open(f"{save_dir}/full_dataset_info.json", "w") as file:
        json.dump(
            {
                "start_time": str(start_time),
                "end_time": str(end_time),
                "input_columns": input_columns,
                "info_columns": info_columns,
            },
            file,
        )
