import pandas as pd
import tensorflow.keras as keras

import numpy as np
from dataclasses import dataclass, asdict
from typing import Dict, Tuple, List
from ast import literal_eval
import pickle
import re


@dataclass
class ModelMetadata:
    network_params: Dict
    training_params: Dict
    val_loss: float
    loss: float
    input_shape: int
    training_time: str
    metrics: Dict
    input_columns: List[str]
    model_type: str

    def to_dict(self):
        return asdict(self)

    def __getitem__(self, item):
        return asdict(self)[item]


def process_model_to_db(
    model: keras.models.Sequential, metadata: ModelMetadata
) -> Tuple[Dict, bytes, Dict]:
    """
    Function processes keras model to data acceptable by database.

    :param model: Keras model.
    :param metadata: Object ModelMetadata class.
    :return: Returns model config, model weights and metadata
    """
    model_config = model.to_json()
    model_weights = pickle.dumps(model.get_weights())
    return model_config, model_weights, metadata.to_dict()


def process_db_to_model(
    model_structure: Dict, weights: pickle
) -> Tuple[keras.models.Sequential, ModelMetadata]:
    """
    Function processes database model data to keras model object and summary about it.
    :param model_structure: structure and additional info about model
    :param weights: keras model weights
    :return: keras model, model_info in pandas DataFrame
    """
    model = keras.models.model_from_json(model_structure["structure"])
    model.set_weights(pickle.loads(weights))
    model_info = _extract_model_info(model_structure)

    return model, model_info


def _extract_model_info(model_structure: Dict) -> ModelMetadata:
    """
    Function casts specific model_info_dict values to proper types
    """
    columns_to_eval = list(ModelMetadata.__dataclass_fields__)
    model_info = {k: v for k, v in model_structure.items() if k in columns_to_eval}

    for column in columns_to_eval:
        if model_info[column]:
            try:
                model_info[column] = literal_eval(model_info[column])
            except (ValueError, SyntaxError):
                pass

    return ModelMetadata(**model_info)


def process_str_array_to_nd(array: str) -> np.ndarray:
    array = re.sub("\s", ",", array)
    array = re.sub(",+", ",", array)
    array = re.sub("^\[,", "[", array)
    return np.asarray(literal_eval(array))


def normalize_columns(stats_df: pd.DataFrame, input_columns: List[str]) -> pd.DataFrame:
    for col in input_columns:
        stats_df[col] = stats_df[col].apply(
            _min_max_normalization,
            max_value=stats_df[col].max(),
            min_value=stats_df[col].min(),
        )
    return stats_df


def normalize_array(v: np.ndarray) -> np.ndarray:
    norm = np.linalg.norm(v)
    if norm == 0:
        return v
    return v / norm


def _min_max_normalization(value: float, max_value: float, min_value: float) -> float:
    return (value - min_value) / (max_value - min_value)


def _mean_normalization(value: float, mean: float, std: float) -> float:
    return (value - mean) / std
