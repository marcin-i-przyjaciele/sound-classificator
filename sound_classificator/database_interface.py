import pymongo
import gridfs
import datetime

from typing import List, Dict, Union, Any, Tuple
from urllib.parse import quote_plus


class DatabaseInterface:
    def __init__(
        self,
        database_name: str = "Default_DB",
        database_collection: str = "Default_collection",
        address: str = "70.37.66.173:27017",
        grid_database: str = "model_database",
        structure_collection: str = "structure_collection",
    ):
        self._user = "root"
        self._password = "dzien_dolby"
        self._database_name = database_name
        self._database_collection = database_collection
        self._address = address
        self._client = pymongo.MongoClient(
            f"mongodb://{quote_plus(self._user)}:{quote_plus(self._password)}@{self._address}"
        )
        self._db = self._client[database_name]
        self._collection = self._db[database_collection]
        self._structure_collection = self._db[structure_collection]
        self._grid = gridfs.GridFS(self._client[grid_database])

    def __str__(self):
        return f"database_name: {self._database_name}, database_collection: {self._database_collection}, address: {self._address}"

    def insert_data(self, data: List[Dict]) -> pymongo.results.InsertManyResult:
        return self._collection.insert_many(data)

    def fetch_data(self, limit: int = 0, skip: int = 0) -> List[Dict]:
        return list(self._collection.find(limit=limit, skip=skip))

    def update_where(
        self, where_column: str, where_value: str, column: str, set_value: str
    ) -> pymongo.results.UpdateResult:
        query = {where_column: where_value}
        new_values = {"$set": {column: set_value}}
        return self._collection.update_many(query, new_values)

    def remove_where(
        self, where_column: str, where_value: str
    ) -> pymongo.results.DeleteResult:
        query = {where_column: where_value}
        return self._collection.delete_many(query)

    def insert_model(
        self,
        model_config: Dict,
        model_weights: bytes,
        metadata: Dict,
        name=None,
        fold_results: Dict = None,
    ) -> pymongo.results.InsertOneResult:
        """
        Inserts keras model into the database.
        It expects model split into config and weights.
        Model id in database is current date if not provided.
        Can raise Value Error if model with the specified name already exists.

        :param model_config:    Configuration part of the model being saved
        :param model_weights:   Weights part of the model being saved
        :param metadata:        Metadata of the model to be included in the db
        :param name:            Name of the model, if none it's current date with
                                precision to seconds
        :param fold_results:    Results of 10-fold training
        :return:                InsertOneResult
        """

        if name is not None and name in self.list_models():
            raise ValueError("Model with that name already exists in the database")

        if name is None:
            name = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

        structure_result = self._insert_structure(
            structure=model_config,
            filename=name,
            metadata=metadata,
            fold_results=fold_results,
        )
        self._insert_weights(weight_object=model_weights, filename=name)
        return structure_result

    def _insert_structure(
        self, structure: Dict, filename: str, metadata: Dict, fold_results: None
    ) -> pymongo.results.InsertOneResult:
        """
        Inserts model structure to database.

        :param structure:       Structure of the model
        :param filename:        Name of the model
        :param metadata:        Metadata to be included with the model
        :param fold_results:    Results of 10-fold training
        """
        data = {
            "filename": filename,
            "structure": structure,
            "fold_results": fold_results,
        }
        data.update(metadata)
        return self._structure_collection.insert_one(data)

    def _insert_weights(self, weight_object: bytes, filename: str) -> None:
        """
        Inserts model weights to database

        :param weight_object:  Weights of the model
        :param filename:       Name of the model
        :return:               None
        """
        new_file = self._grid.new_file(filename=filename)
        new_file.write(weight_object)
        new_file.close()

    def delete_model(self, model_name: str) -> pymongo.results.DeleteResult:
        """
        Deletes the model specified if it exists in database.
        Raises ValueError if the model is not in the db.

        :param model_name:  Name of the model to fetch
        :return:            DeleteOneResult
        """

        if model_name not in self.list_models():
            raise ValueError("Model does not exist")
        cursor = self._grid.find_one({"filename": model_name})
        self._grid.delete(cursor._id)
        return self._structure_collection.delete_one({"filename": model_name})

    def list_models(self) -> List[str]:
        """
        Returns list of all models inserted into database.
        """
        return [model.filename for model in self._grid.find({})]

    def fetch_model(self, model_name: str) -> Tuple[Dict, bytes]:
        """
        Fetches keras model from database.
        Gets model structure and weights from database separately.

        :param model_name:  Name of the model to fetch
        :return:            (model_structure, weights)
        """

        if model_name not in self.list_models():
            raise ValueError("Model does not exist")
        weights = self._fetch_weights(model_name)
        model_structure = self._fetch_model_structure(model_name)
        return model_structure, weights

    def _fetch_weights(self, model_name: str):
        """
        Fetches model weights from database.
        """
        out = self._grid.find_one({"filename": model_name})
        return out.read()

    def _fetch_model_structure(self, model_name: str):
        """
        Fetches model structure from database.
        """
        return self._structure_collection.find_one({"filename": model_name})

    def _fetch_all_structures(self) -> List[Dict]:
        """
        Returns structure of every model in the database.
        """
        return list(self._structure_collection.find({}))

    def fetch_model_summary(self) -> List[Dict]:
        """
        Returns summary of every model in the database.
        """
        all_structures = self._fetch_all_structures()
        pop_keys = ["structure", "_id"]

        for info in all_structures:
            for k in pop_keys:
                info.pop(k, None)

        return all_structures
