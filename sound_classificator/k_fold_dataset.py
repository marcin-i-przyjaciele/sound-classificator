import pandas as pd
from typing import Tuple, List

import os
import json
from audio_processing import load_data, replace_audio_with_stats
from data_processing import normalize_columns


class KFoldDataset:
    """
    Iterator for given k-fold data distribution.
    Returns tuples in form of (test_data, train_data)
    """

    def __init__(self, metadata: pd.DataFrame, key_informations: List[str] = None):
        self.metadata = metadata
        self.number_of_folds = len(self.metadata.fold.unique())
        self.starting_fold = 1
        self.data_loaded = False
        self.key_informations = key_informations

        self.data = None
        self.input_columns = None
        self.info_columns = None

    def __len__(self):
        return self.number_of_folds

    def __getitem__(self, item) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        fold = item % (self.number_of_folds + 1) is written that way
        ensure folds are the same as in dataset. (Numbered from 1)
        """
        if not self.data_loaded:
            raise ValueError("Audio files are not loaded yet")

        fold = item % (self.number_of_folds + 1)
        test_data = self.data.query("fold == @fold")
        train_data = self.data.query("fold != @fold")

        return test_data, train_data

    def __iter__(self):
        self.current_fold = self.starting_fold
        return self

    def __next__(self):
        if self.current_fold <= self.number_of_folds:
            data = self[self.current_fold]
            self.current_fold += 1
            return data
        else:
            self.current_fold = self.starting_fold
            raise StopIteration

    def load_data(self, normalize_data: bool = True):
        self.data, self.input_columns, self.info_columns = replace_audio_with_stats(
            load_data(self.metadata)
        )

        if normalize_data:
            self.data = normalize_columns(self.data, self.input_columns)

        self.data_loaded = True

    def load_preprocessed_data(
        self, path: str, normalize_data: bool = True, input_columns: List[str] = None
    ):
        self.data = pd.read_csv(
            os.path.join(path, "full_dataset.csv"), index_col="index"
        )

        with open(os.path.join(path, "full_dataset_info.json")) as file:
            info = json.load(file)
        self.input_columns = (
            info["input_columns"] if not input_columns else input_columns
        )
        self.info_columns = info["info_columns"]

        if normalize_data:
            columns_to_normalize = [
                col for col in self.input_columns if "CNN" not in col.upper()
            ]
            self.data = normalize_columns(self.data, columns_to_normalize)

        self.data_loaded = True
