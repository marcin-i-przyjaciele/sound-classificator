import librosa as lr
import pandas as pd
import numpy as np
from typing import List


def extract_features(
    audio_df: pd.DataFrame, key_informations: List[str] = None
) -> pd.DataFrame:
    features = pd.DataFrame()

    features["mean"] = extract_mean(audio_df)
    features["std"] = extract_std(audio_df)
    features["median"] = extract_median(audio_df)

    mfccs_stats = statistics(extract_mfccs(audio_df))
    melspectrogram_stats = statistics(extract_melspectrogram(audio_df))
    rms_stats = statistics(extract_rms(audio_df))

    informations_df = pd.concat(
        [features, mfccs_stats, melspectrogram_stats, rms_stats], axis=1
    )
    columns = key_informations if key_informations else informations_df.columns

    return informations_df[columns]


def extract_mean(audio_df: pd.DataFrame) -> pd.Series:
    mean_series = audio_df["audio"].apply(np.mean)
    return mean_series


def extract_std(audio_df: pd.DataFrame) -> pd.Series:
    std_series = audio_df["audio"].apply(np.std)
    return std_series


def extract_median(audio_df: pd.DataFrame) -> pd.Series:
    median_series = audio_df["audio"].apply(np.median)
    return median_series


def extract_rms(audio_df: pd.DataFrame) -> pd.Series:
    rms_series = audio_df["audio"].apply(lr.feature.rms)
    rms_series.name = "rms_series"
    return rms_series


def extract_spectral_bandwith(audio_df: pd.DataFrame) -> pd.Series:
    bandwidth = audio_df["audio"].apply(
        lambda y: lr.feature.spectral_bandwidth(y=y, sr=44100)
    )
    bandwidth.name = "bandwidth"
    return bandwidth


def extract_melspectrogram(audio_df: pd.DataFrame) -> pd.Series:
    melspectrogram = audio_df.audio.apply(
        lambda audio: lr.feature.melspectrogram(audio, sr=44100)
    )
    melspectrogram.name = "melspectrogram"
    return melspectrogram


def extract_mfccs(audio_df: pd.DataFrame) -> pd.Series:
    mfccs = audio_df.audio.apply(lambda audio: lr.feature.mfcc(audio, sr=44100))
    mfccs.name = "mfccs"
    return mfccs


def extract_flattnes(audio_df: pd.DataFrame) -> pd.Series:
    flatness = audio_df.audio.apply(lambda audio: lr.feature.spectral_flatness(y=audio))
    flatness.name = "flatness"
    return flatness


def extract_spectral_contrast(audio_df: pd.DataFrame) -> pd.Series:
    spectral_contrast = audio_df.audio.apply(
        lambda audio: lr.feature.spectral_contrast(audio, sr=44100)
    )
    spectral_contrast.name = "spectral_contrast"
    return spectral_contrast


def extract_spectral_rolloff(audio_df: pd.DataFrame) -> pd.Series:
    spectral_rolloff = audio_df.audio.apply(
        lambda audio: lr.feature.spectral_rolloff(audio, sr=44100)
    )
    spectral_rolloff.name = "spectral_rolloff"
    return spectral_rolloff


def cnn_mean(values: pd.Series) -> np.ndarray:
    return np.mean(values.T, axis=0)


def statistics(
    extracted_values: pd.Series, bunch_of_functions: list = None
) -> pd.DataFrame:
    if not bunch_of_functions:
        bunch_of_functions = [np.min, np.max, np.std, np.median, np.mean, cnn_mean]
    name = extracted_values.name
    return pd.DataFrame(
        {
            f"{name}_{function.__name__}": extracted_values.apply(function)
            for function in bunch_of_functions
        }
    )
