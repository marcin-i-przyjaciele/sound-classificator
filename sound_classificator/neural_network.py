import tensorflow.keras as keras
import pandas as pd
import numpy as np
import time
from typing import List, Dict, Tuple, Union
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import confusion_matrix, classification_report
from dataclasses import dataclass, asdict, field
from sound_classificator.data_processing import (
    ModelMetadata,
    process_str_array_to_nd,
    normalize_array,
)


@dataclass
class NetworkParameters:
    hidden_layer_sizes: List[int] = None
    optimizer: str = "nadam"
    loss: str = "categorical_crossentropy"
    dropout_rate: float = 0.1
    kernel_initializer: str = "lecun_normal"
    activation_layer: str = "selu"
    output_activation_layer: str = "softmax"
    pool_size: Tuple = tuple([2, 2])
    conv_kernel_size: Tuple = tuple([3, 3])

    def __post_init__(self):
        if not self.hidden_layer_sizes:
            self.hidden_layer_sizes = [8, 4]

    def to_dict(self):
        return asdict(self)

    def __getitem__(self, item):
        return self.to_dict()[item]


@dataclass
class TrainingParameters:
    epochs: int = 500
    batch_size: int = 32
    early_stop: bool = True
    training_time: bool = True
    normalize_data: bool = False
    input_columns: List[str] = list
    cnn_shape: Tuple[int] = None

    def to_dict(self):
        return asdict(self)

    def __getitem__(self, item):
        return self.to_dict()[item]


def create_model_from_parameters(
    input_dim: Tuple,
    output_shape: int,
    dropout_rate: float,
    hidden_layer_sizes: List[int],
    optimizer: str,
) -> keras.models.Sequential:
    network_params = NetworkParameters(
        dropout_rate=dropout_rate,
        hidden_layer_sizes=hidden_layer_sizes,
        optimizer=optimizer,
    )
    return create_sequential_model(input_dim, output_shape, network_params)


def create_sequential_model(
    input_dim: Tuple, output_shape: int, network_params: NetworkParameters
) -> keras.models.Sequential:
    """
    Creates simple sequential model based on RNN architecture.
    """
    keras.backend.clear_session()

    model = keras.models.Sequential()
    model.add(keras.Input(shape=input_dim))

    for layer_size in network_params.hidden_layer_sizes:
        model.add(
            keras.layers.Dense(
                layer_size,
                kernel_initializer=network_params.kernel_initializer,
                activation=network_params.activation_layer,
            )
        )
        model.add(keras.layers.AlphaDropout(network_params.dropout_rate))

    model.add(
        keras.layers.Dense(
            output_shape,
            kernel_initializer=network_params.kernel_initializer,
            activation=network_params.output_activation_layer,
        )
    )
    model.compile(
        loss=network_params.loss,
        optimizer=network_params.optimizer,
        metrics=create_metrics(),
    )

    return model


def create_cnn_model(
    input_dim: Tuple, output_shape: int, network_params: NetworkParameters
) -> keras.models.Sequential:
    """
    Creates simple sequential model based on RNN architecture.
    """
    keras.backend.clear_session()

    model = keras.models.Sequential()
    model.add(keras.Input(shape=input_dim))

    for layer_size in network_params.hidden_layer_sizes[:-1]:
        model.add(
            keras.layers.Conv2D(
                layer_size,
                network_params.conv_kernel_size,
                padding="same",
                activation=network_params.activation_layer,
            )
        )
        model.add(keras.layers.MaxPool2D(pool_size=network_params.pool_size))

    model.add(keras.layers.Dropout(network_params.dropout_rate))
    model.add(keras.layers.Flatten())
    model.add(
        keras.layers.Dense(
            network_params.hidden_layer_sizes[-1],
            activation=network_params.activation_layer,
        )
    )
    model.add(
        keras.layers.Dense(
            output_shape, activation=network_params.output_activation_layer
        )
    )
    model.compile(
        loss=network_params.loss,
        optimizer=network_params.optimizer,
        metrics=create_metrics(),
    )

    return model


def data_pipeline(
    data_df: pd.DataFrame,
    encoder: LabelBinarizer,
    input_columns: List[str],
    cnn_shape: Tuple[int],
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Processes objects from audio format to form acceptable by the neural network.
    """

    data_df = data_df.reset_index(drop=True)
    Y = encoder.transform(data_df["class"])
    X = data_df[input_columns]

    for col in input_columns:
        if "CNN" in col.upper():
            X[col] = X[col].apply(process_str_array_to_nd)
            X[col] = X[col].apply(normalize_array)

    if len(X.columns) == 1:
        X = X[X.columns[0]]

    if cnn_shape:
        X = np.stack(X.apply(lambda x: x.reshape(cnn_shape)))
    elif isinstance(X, pd.Series):
        X = np.stack(X[X.columns[0]])

    return X, Y


def train_on_given_data(
    train_data: pd.DataFrame,
    test_data: pd.DataFrame,
    encoder: LabelBinarizer,
    input_columns: List[str],
    model_type: str,
    network_params: NetworkParameters = None,
    training_params: TrainingParameters = None,
) -> Tuple[keras.models.Sequential, keras.callbacks.History, ModelMetadata]:
    """
    Creates and trains model on given data.
    """
    if not network_params:
        network_params = NetworkParameters()
    if not training_params:
        training_params = TrainingParameters()

    callbacks = create_callbacks(training_params)
    if model_type == "FNN":
        create_model = create_sequential_model
    elif model_type == "CNN":
        create_model = create_cnn_model
    else:
        raise ValueError("Invalid model_type!")

    X_train, Y_train = data_pipeline(
        train_data, encoder, input_columns, training_params["cnn_shape"]
    )
    X_test, Y_test = data_pipeline(
        test_data, encoder, input_columns, training_params["cnn_shape"]
    )

    input_dim = (
        X_train.iloc[0].shape if isinstance(X_train, pd.DataFrame) else X_train[0].shape
    )
    output_shape = Y_train.shape[1]

    model = create_model(
        input_dim=input_dim, output_shape=output_shape, network_params=network_params
    )

    history = model.fit(
        X_train,
        Y_train,
        validation_data=(X_test, Y_test),
        epochs=training_params.epochs,
        batch_size=training_params.batch_size,
        callbacks=list(callbacks.values()),
    )

    metadata = create_metadata(
        network_params=network_params,
        training_params=training_params,
        history=history,
        input_dim=input_dim,
        callbacks=callbacks,
        input_columns=input_columns,
        model_type=model_type,
    )

    return model, history, metadata


def create_class_mapping(metadata_df: pd.DataFrame) -> Dict[str, int]:
    """
    Creates mapping for classID and class columns.
    """
    return dict(sorted(zip(metadata_df["classID"], metadata_df["class"])))


def model_prediction(
    model: keras.models.Sequential,
    data_df: pd.DataFrame,
    encoder: LabelBinarizer,
    input_columns: List[str],
    cnn_shape: Tuple[int],
    mapping: Dict[int, str] = None,
) -> pd.DataFrame:
    """
    Creates prediction on given data using an already existing model.
    """

    X, Y = data_pipeline(data_df, encoder, input_columns, cnn_shape)
    prediction = model.predict(X)

    results = pd.DataFrame(prediction, columns=sorted(mapping.keys()))
    results["prediction_classID"] = [res.argmax() for res in prediction]
    results["prediction_class"] = results.prediction_classID.map(mapping)

    return results


def predict_and_analyze(
    model: keras.Sequential,
    data_df: pd.DataFrame,
    encoder: LabelBinarizer,
    input_columns: List[str],
    cnn_shape: Tuple[int],
    mapping: Dict[int, str] = None,
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Creates prediction and analysis on given data.
    Returns results with true and predicted classes, classification report and confusion matrix.
    """
    if not mapping:
        mapping = create_class_mapping(data_df)

    prediction = model_prediction(
        model, data_df, encoder, input_columns, cnn_shape, mapping
    )
    classes = list(mapping.values())

    results = data_df.join(prediction)
    results_cols = [
        "salience",
        "fold",
        "classID",
        "class",
        "prediction_classID",
        "prediction_class",
    ]

    conf_matrix, report = create_analysis(results, classes)

    return results[results_cols], report, conf_matrix


def create_analysis(
    results: pd.DataFrame, classes: List[str]
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    report = pd.DataFrame(
        classification_report(
            results["classID"],
            results["prediction_classID"],
            target_names=classes,
            output_dict=True,
        )
    )
    conf_matrix = pd.DataFrame(
        confusion_matrix(results["classID"], results["prediction_classID"]),
        index=classes,
        columns=classes,
    )
    return conf_matrix, report


def create_metadata(
    network_params: NetworkParameters,
    training_params: TrainingParameters,
    history: keras.callbacks.History,
    input_dim: Union[Tuple, int],
    callbacks: Dict[str, keras.callbacks.Callback],
    input_columns: List[str],
    model_type: str,
) -> ModelMetadata:

    loss = float(history.history["loss"][-1])
    val_loss = float(history.history["val_loss"][-1])
    metrics = {
        metric: float(values[-1])
        for metric, values in history.history.items()
        if metric not in ["loss", "val_loss"]
    }

    input_shape = input_dim[0] if isinstance(input_dim, tuple) else input_dim
    training_time_seconds = callbacks["training_time"].get_total_training_time()
    training_time = pd.Timedelta(training_time_seconds)

    return ModelMetadata(
        network_params=network_params.to_dict(),
        training_params=training_params.to_dict(),
        loss=loss,
        val_loss=val_loss,
        input_shape=input_shape,
        training_time=str(training_time),
        metrics=metrics,
        input_columns=input_columns,
        model_type=model_type,
    )


class TrainingTime(keras.callbacks.Callback):
    def on_train_begin(self, logs=None):
        self.times = []
        self.training_start = pd.Timestamp.now()

    def on_train_end(self, logs=None):
        self.training_end = pd.Timestamp.now()

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch_time_start = pd.Timestamp.now()

    def on_epoch_end(self, epoch, logs=None):
        self.times.append(pd.Timestamp.now() - self.epoch_time_start)

    def get_total_training_time(self):
        return self.training_end - self.training_start

    def get_epochs_times(self):
        return self.times


def create_callbacks(args) -> Dict[str, keras.callbacks.Callback]:
    callbacks = {}
    if args.early_stop:
        callbacks["early_stop"] = keras.callbacks.EarlyStopping(
            monitor="val_loss", patience=30, verbose=1
        )

    if args.training_time:
        callbacks["training_time"] = TrainingTime()

    return callbacks


def create_metrics() -> List[str]:
    return ["categorical_accuracy", "categorical_crossentropy"]
