import os
import pandas as pd
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import LabelBinarizer
from typing import List, Dict
from math import log2
from ast import literal_eval

from sound_classificator.k_fold_dataset import KFoldDataset
from sound_classificator.neural_network import (
    create_model_from_parameters,
    create_class_mapping,
)

from tools import training_script

METADATA_PATH = r"UrbanSound8K\UrbanSound8K.csv"
PREPROCESSED_DATA_PATH = r".\preprocessed_data"
NORMALIZE_DATA = True

OPTIMIZERS = ["rmsprop", "adagrad", "adadelta", "adam"]
TOP_HIDDEN_LAYER_SIZES = [512]
DROPOUT_RATES = [0.1, 0.2, 0.3, 0.4, 0.5]
NUMBER_OF_MODELS = 10
GRID_EPOCHS = 100
TRAIN_EPOCHS = 500
UPLOAD_TO_DB = True
N_JOBS = -1


def keras_gridsearch() -> str:
    full_path = os.path.join(os.getcwd(), METADATA_PATH)

    metadata_df = pd.read_csv(full_path)
    mapping = create_class_mapping(metadata_df)

    encoder = LabelBinarizer()
    encoder.fit(list(mapping.values()))

    path = os.path.join(os.getcwd(), PREPROCESSED_DATA_PATH)

    dataset = KFoldDataset(metadata_df)
    dataset.load_preprocessed_data(path, normalize_data=NORMALIZE_DATA)

    HIDDEN_LAYER_SIZES = generate_size_proposals(TOP_HIDDEN_LAYER_SIZES)

    X = dataset.data[dataset.input_columns]
    Y = encoder.transform(dataset.data["class"])
    input_dim = X.iloc[0].shape
    output_shape = Y.shape[1]
    del dataset

    NN_grid = KerasRegressor(
        build_fn=create_model_from_parameters,
        batch_size=32,
        epochs=GRID_EPOCHS,
        verbose=1,
    )

    validator = GridSearchCV(
        estimator=NN_grid,
        param_grid={
            "input_dim": [input_dim],
            "output_shape": [output_shape],
            "hidden_layer_sizes": HIDDEN_LAYER_SIZES,
            "optimizer": OPTIMIZERS,
            "dropout_rate": DROPOUT_RATES,
        },
        cv=10,
        n_jobs=N_JOBS,
    )

    grid_results = validator.fit(X, Y)

    grid_params = pd.DataFrame(grid_results.cv_results_)

    date = str(pd.Timestamp.now().floor("S"))
    file_name = f"grid_results_{date}.csv"
    grid_params.to_csv(file_name, index=False)

    return file_name


def _generate_decreasing_power_list(
    start_n: int, descending: bool = True
) -> List[List[int]]:
    """
    f(32) ----> [[32], [32, 16], [32, 16, 8], [32, 16, 8, 4], [32, 16, 8, 4, 2]]
    """

    numbers = sorted(
        {int(pow(2, x)) for x in range(0, int(log2(start_n) + 1))}, reverse=descending
    )
    results = []
    for n in range(1, len(numbers)):
        results.append(tuple(numbers[:n]))
    return results


def generate_size_proposals(biggest_layer_sizes=None) -> List[List[int]]:
    """
    Generates hidden layers' sizes proposals.
    Every next layer generated this way is twice smaller than the previous one.

    :param biggest_layer_sizes: Every number in the list represents the number of neurons in the biggest layers.
    :return:                    List of possible hidden layer sizes.
    """
    if biggest_layer_sizes is None:
        biggest_layer_sizes = []
    results = []
    for n in biggest_layer_sizes:
        results.extend(_generate_decreasing_power_list(n))
    return results


def train_gridsearch_results(file_name: str):
    grid_params = pd.read_csv(file_name)
    grid_params = grid_params[grid_params.rank_test_score <= NUMBER_OF_MODELS]
    list_of_params = grid_params.apply(_extract_params, axis=1).values

    for params in list_of_params:
        training_script.NETWORK_PARAMS.update(params)
        training_script.main()


def _extract_params(row: pd.Series) -> Dict:
    params = literal_eval(row["params"])
    params["hidden_layer_sizes"] = list(params["hidden_layer_sizes"])

    del params["input_dim"]
    del params["output_shape"]

    return params


def main():
    results_file_name = keras_gridsearch()
    train_gridsearch_results(results_file_name)


if __name__ == "__main__":
    main()
