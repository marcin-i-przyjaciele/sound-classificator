import pandas as pd
from sklearn.preprocessing import LabelBinarizer
import os

from sound_classificator.k_fold_dataset import KFoldDataset
from sound_classificator.database_interface import DatabaseInterface
from sound_classificator.data_processing import process_db_to_model
from sound_classificator.neural_network import create_class_mapping, predict_and_analyze

MODEL_TYPE = "CNN"

PARAMS = {
    "CNN": {
        "TRAIN_PARAMS": {
            "epochs": 500,
            "batch_size": 32,
            "early_stop": True,
            "training_time": True,
            "normalize_data": True,
            "input_columns": ["melspectrogram_cnn_mean"],
            "cnn_shape": (16, 8, 1),
        },
        "NETWORK_PARAMS": {
            "hidden_layer_sizes": [64, 128, 1024],
            # Last layer is Dense type
            "optimizer": "nadam",
            "loss": "categorical_crossentropy",
            "dropout_rate": 0.1,
            "kernel_initializer": None,
            "activation_layer": "tanh",
            "output_activation_layer": "softmax",
            "pool_size": (2, 2),
            "conv_kernel_size": (3, 3),
        },
    },
    "FNN": {
        "TRAIN_PARAMS": {
            "epochs": 500,
            "batch_size": 32,
            "early_stop": True,
            "training_time": True,
            "normalize_data": True,
            "input_columns": None,
            "cnn_shape": None,
        },
        "NETWORK_PARAMS": {
            "hidden_layer_sizes": [32, 16, 8, 4, 2, 1],
            "optimizer": "nadam",
            "loss": "categorical_crossentropy",
            "dropout_rate": 0.1,
            "kernel_initializer": "lecun_normal",
            "activation_layer": "selu",
            "output_activation_layer": "softmax",
            "pool_size": None,
            "conv_kernel_size": None,
        },
    },
}

UPLOAD_LAST_MODEL_TO_DB = True
MODEL_NAME = None

FRACTION_OF_DATA_TO_USE = 1.0
METADATA_PATH = r"UrbanSound8K\UrbanSound8K.csv"

USE_PREPROCESSED_DATA = True
PREPROCESSED_DATA_PATH = r".\preprocessed_data"


TRAIN_PARAMS = PARAMS[MODEL_TYPE]["TRAIN_PARAMS"]
NETWORK_PARAMS = PARAMS[MODEL_TYPE]["NETWORK_PARAMS"]

full_path = os.path.join(os.getcwd(), METADATA_PATH)

metadata_df = pd.read_csv(full_path)
mapping = create_class_mapping(metadata_df)

encoder = LabelBinarizer()
encoder.fit(list(mapping.values()))

if USE_PREPROCESSED_DATA:
    path = os.path.normpath(os.path.join(os.getcwd(), PREPROCESSED_DATA_PATH))
    dataset = KFoldDataset(metadata_df)
    dataset.load_preprocessed_data(
        path, TRAIN_PARAMS["normalize_data"], TRAIN_PARAMS["input_columns"]
    )
else:
    dataset = KFoldDataset(metadata_df.sample(frac=FRACTION_OF_DATA_TO_USE))
    dataset.load_data(TRAIN_PARAMS["normalize_data"])

db = DatabaseInterface()
summary = pd.DataFrame(db.fetch_model_summary())

MODEL_ID = -1
model, info = process_db_to_model(*db.fetch_model(summary.filename.iloc[MODEL_ID]))

results, report, conf_matrix = predict_and_analyze(
    model,
    dataset.data,
    encoder,
    info.training_params["input_columns"],
    info.training_params["cnn_shape"],
    mapping,
)
