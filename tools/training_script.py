import pandas as pd
import tensorflow.keras as keras
from sklearn.preprocessing import LabelBinarizer
from typing import List
import os
import json

from sound_classificator.k_fold_dataset import KFoldDataset
from sound_classificator.database_interface import DatabaseInterface
from sound_classificator.data_processing import process_model_to_db, ModelMetadata
from sound_classificator.neural_network import (
    TrainingParameters,
    train_on_given_data,
    create_class_mapping,
    NetworkParameters,
)

MODEL_TYPE = "CNN"

PARAMS = {
    "CNN": {
        "TRAIN_PARAMS": {
            "epochs": 500,
            "batch_size": 32,
            "early_stop": True,
            "training_time": True,
            "normalize_data": True,
            "input_columns": ["melspectrogram_cnn_mean"],
            "cnn_shape": (16, 8, 1),
        },
        "NETWORK_PARAMS": {
            "hidden_layer_sizes": [64, 128, 1024],
            # Last layer is Dense type
            "optimizer": "nadam",
            "loss": "categorical_crossentropy",
            "dropout_rate": 0.1,
            "kernel_initializer": None,
            "activation_layer": "tanh",
            "output_activation_layer": "softmax",
            "pool_size": (2, 2),
            "conv_kernel_size": (3, 3),
        },
    },
    "FNN": {
        "TRAIN_PARAMS": {
            "epochs": 500,
            "batch_size": 32,
            "early_stop": True,
            "training_time": True,
            "normalize_data": True,
            "input_columns": None,
            "cnn_shape": None,
        },
        "NETWORK_PARAMS": {
            "hidden_layer_sizes": [32, 16, 8, 4, 2, 1],
            "optimizer": "nadam",
            "loss": "categorical_crossentropy",
            "dropout_rate": 0.1,
            "kernel_initializer": "lecun_normal",
            "activation_layer": "selu",
            "output_activation_layer": "softmax",
            "pool_size": None,
            "conv_kernel_size": None,
        },
    },
}

UPLOAD_LAST_MODEL_TO_DB = True
MODEL_NAME = None

FRACTION_OF_DATA_TO_USE = 1.0
METADATA_PATH = r"UrbanSound8K\UrbanSound8K.csv"

USE_PREPROCESSED_DATA = True
PREPROCESSED_DATA_PATH = r".\preprocessed_data"


def main():
    TRAIN_PARAMS = PARAMS[MODEL_TYPE]["TRAIN_PARAMS"]
    NETWORK_PARAMS = PARAMS[MODEL_TYPE]["NETWORK_PARAMS"]

    full_path = os.path.join(os.getcwd(), METADATA_PATH)

    metadata_df = pd.read_csv(full_path)
    mapping = create_class_mapping(metadata_df)

    encoder = LabelBinarizer()
    encoder.fit(list(mapping.values()))

    if USE_PREPROCESSED_DATA:
        path = os.path.normpath(os.path.join(os.getcwd(), PREPROCESSED_DATA_PATH))
        dataset = KFoldDataset(metadata_df)
        dataset.load_preprocessed_data(
            path, TRAIN_PARAMS["normalize_data"], TRAIN_PARAMS["input_columns"]
        )
    else:
        dataset = KFoldDataset(metadata_df.sample(frac=FRACTION_OF_DATA_TO_USE))
        dataset.load_data(TRAIN_PARAMS["normalize_data"])

    training_params = TrainingParameters(**TRAIN_PARAMS)
    network_params = NetworkParameters(**NETWORK_PARAMS)
    model_results = []

    db = DatabaseInterface()

    for test_data, train_data in dataset:
        model, history, metadata = train_on_given_data(
            train_data=train_data,
            test_data=test_data,
            training_params=training_params,
            network_params=network_params,
            encoder=encoder,
            input_columns=dataset.input_columns,
            model_type=MODEL_TYPE,
        )

        model_results.append(metadata)

    fold_results = _create_fold_results(model_results)
    fold_results.to_csv("fold_results.csv")
    with open("model_metadata.json", "w") as file:
        json.dump(metadata.to_dict(), file)

    if UPLOAD_LAST_MODEL_TO_DB:
        db_config, db_weights, db_meta = process_model_to_db(model, metadata)
        db_fold_results = fold_results.rename(
            {i: f"fold_{i}" for i in range(1, 11)}, axis=1
        ).to_dict()
        db.insert_model(
            db_config,
            db_weights,
            db_meta,
            name=MODEL_NAME,
            fold_results=db_fold_results,
        )


def _create_fold_results(model_results: List[ModelMetadata]) -> pd.DataFrame:
    return pd.DataFrame(
        {i + 1: metadata["metrics"] for i, metadata in enumerate(model_results)}
    )


if __name__ == "__main__":
    main()
